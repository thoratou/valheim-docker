SHELL := /bin/bash
DOCKER_ID = $(shell docker ps | grep "valheim_server_test" | awk '{print $$1}')

.PHONY: all
all: build down up

.PHONY: build
build:
	@docker build -t valheim-server .

.PHONY: up
up:
	@docker-compose -f local/docker-compose.yml up -d --force-recreate

.PHONY: down
down:
	@docker-compose -f local/docker-compose.yml down

.PHONY: bash
bash:
	@if [ -z "$(DOCKER_ID)" ]; then \
		echo "container not started"; \
	else \
		docker exec -it $(DOCKER_ID) bash; \
	fi

.PHONY: log
log:
	@if [ -z "$(DOCKER_ID)" ]; then \
		echo "container not started"; \
	else \
		docker logs $(DOCKER_ID); \
	fi

.PHONY: tail-log
tail-log:
	@if [ -z "$(DOCKER_ID)" ]; then \
		echo "container not started"; \
	else \
		docker logs -f $(DOCKER_ID); \
	fi
