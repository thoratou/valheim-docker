FROM cm2network/steamcmd

LABEL maintainer "thoratou"

## Dependencies ##

USER root
RUN apt-get update && apt-get install -y procps

## App ##

WORKDIR /home/steam
USER steam
COPY --chown=steam start.sh /home/steam/start.sh

ENTRYPOINT sh start.sh